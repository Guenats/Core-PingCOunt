﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PingCount.Models
{
    public class Counter
    {
        public long Id { get; set; }
        public int TotalPing { get; set; }
    }
}
